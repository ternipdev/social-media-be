/** @format */

import Post from '../models/PostModel.js';
import User from '../models/UserModel.js';
import Image from '../models/ImageModel.js';
import * as Upload from '../middlewares/upload.js';

export const postHandle = {
  getUserPost: async (req, res) => {
    try {
      const { id } = req.user;
      const user = await User.findById({ _id: id, isValid: true }).select(
        'following',
      );
      const allPost = await Post.find({ isValid: true })
        .sort({ createdAt: -1 })
        .select('owner');
      console.log(user, allPost);
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  },
  getSinglePost: (req, res) => {
    try {
      const { id } = req.params;
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  },
  addPost: async (req, res) => {
    try {
      let reqBody = req.body;
      const { id } = req.user;
      reqBody.owner = id;

      const savedPost = await Post.create(reqBody);
      await User.findByIdAndUpdate(
        { _id: id, isValid: true },
        {
          $push: {
            posts: {
              $each: [savedPost._id],
            },
          },
        },
        { new: true },
      );

      const listPost = !req.body.callback
        ? await Post.find({ isValid: true })
            .sort({ createdAt: -1 })
            .populate([
              {
                path: 'owner',
                model: 'User',
                populate: {
                  path: 'avatar',
                  model: 'Image',
                },
              },
              {
                path: 'likes',
              },

              {
                path: 'comments',
                populate: {
                  path: 'owner',
                  populate: {
                    path: 'avatar',
                  },
                },
              },
            ])
        : (
            await User.findById(savedPost.owner)
              .sort({ createdAt: -1 })
              .populate([
                {
                  model: 'Image',
                  path: 'avatar',
                },
                {
                  model: 'Image',
                  path: 'cover',
                },
                {
                  model: 'Post',
                  path: 'posts',
                  options: { sort: { createdAt: -1 } },
                  populate: [
                    {
                      path: 'owner',
                      model: 'User',
                      populate: {
                        path: 'avatar',
                        model: 'Image',
                      },
                    },
                    {
                      path: 'likes',
                      model: 'User',
                    },

                    {
                      path: 'comments',
                      populate: {
                        path: 'owner',
                        populate: {
                          path: 'avatar',
                        },
                      },
                    },
                  ],
                },
              ])
          )?.posts;
      res.status(201).json({ msg: 'Create post success !', listPost });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error: error.message });
    }
  },
  updatePost: async (req, res) => {
    try {
      const { id } = req.params;
      let reqBody = req.body;

      const savedPost = await Post.findByIdAndUpdate(id, reqBody);

      const listPost = !req.body.callback
        ? await Post.find({ isValid: true })
            .sort({ createdAt: -1 })
            .populate([
              {
                path: 'owner',
                model: 'User',
                populate: {
                  path: 'avatar',
                  model: 'Image',
                },
              },
              {
                path: 'likes',
              },

              {
                path: 'comments',
                populate: {
                  path: 'owner',
                  populate: {
                    path: 'avatar',
                  },
                },
              },
            ])
        : (
            await User.findById(savedPost.owner)
              .sort({ createdAt: -1 })
              .populate([
                {
                  model: 'Image',
                  path: 'avatar',
                },
                {
                  model: 'Image',
                  path: 'cover',
                },
                {
                  model: 'Post',
                  path: 'posts',
                  options: { sort: { createdAt: -1 } },
                  populate: [
                    {
                      path: 'owner',
                      model: 'User',
                      populate: {
                        path: 'avatar',
                        model: 'Image',
                      },
                    },
                    {
                      path: 'likes',
                      model: 'User',
                    },

                    {
                      path: 'comments',
                      populate: {
                        path: 'owner',
                        populate: {
                          path: 'avatar',
                        },
                      },
                    },
                  ],
                },
              ])
          )?.posts;
      res.status(200).json({ msg: 'Update post success !', listPost });
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  },
  deletePost: async (req, res) => {
    try {
      const { id } = req.params;
      const patchPost = await Post.findByIdAndDelete(id);
      await User.findByIdAndUpdate(
        { _id: req.user.id, isValid: true },
        {
          $pull: {
            posts: id,
          },
        },
      );
      const listPost = !req.body.callback
        ? await Post.find({ isValid: true })
            .sort({ createdAt: -1 })
            .populate([
              {
                path: 'owner',
                model: 'User',
                populate: {
                  path: 'avatar',
                  model: 'Image',
                },
              },
              {
                path: 'likes',
              },

              {
                path: 'comments',
                populate: {
                  path: 'owner',
                  populate: {
                    path: 'avatar',
                  },
                },
              },
            ])
        : (
            await User.findById(patchPost.owner)
              .sort({ createdAt: -1 })
              .populate([
                {
                  model: 'Image',
                  path: 'avatar',
                },
                {
                  model: 'Image',
                  path: 'cover',
                },
                {
                  model: 'Post',
                  path: 'posts',
                  options: { sort: { createdAt: -1 } },
                  populate: [
                    {
                      path: 'owner',
                      model: 'User',
                      populate: {
                        path: 'avatar',
                        model: 'Image',
                      },
                    },
                    {
                      path: 'likes',
                      model: 'User',
                    },

                    {
                      path: 'comments',
                      populate: {
                        path: 'owner',
                        populate: {
                          path: 'avatar',
                        },
                      },
                    },
                  ],
                },
              ])
          )?.posts;
      res.status(200).json({ msg: 'Delete post success !', listPost });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error: error.message });
    }
  },
  likePost: async (req, res) => {
    try {
      const { id } = req.params;

      const patchPost = await Post.findById(id);
      const index = patchPost.likes.indexOf(req.user.id);
      index < 0
        ? patchPost.likes.push(req.user.id)
        : patchPost.likes.splice(index, 1);

      await patchPost.save();

      const listPost = !req.body.callback
        ? await Post.find({ isValid: true })
            .sort({ createdAt: -1 })
            .populate([
              {
                path: 'owner',
                model: 'User',
                populate: {
                  path: 'avatar',
                  model: 'Image',
                },
              },
              {
                path: 'likes',
              },

              {
                path: 'comments',
                populate: {
                  path: 'owner',
                  populate: {
                    path: 'avatar',
                  },
                },
              },
            ])
        : (
            await User.findById(patchPost.owner)
              .sort({ createdAt: -1 })
              .populate([
                {
                  model: 'Image',
                  path: 'avatar',
                },
                {
                  model: 'Image',
                  path: 'cover',
                },
                {
                  model: 'Post',
                  path: 'posts',
                  options: { sort: { createdAt: -1 } },
                  populate: [
                    {
                      path: 'owner',
                      model: 'User',
                      populate: {
                        path: 'avatar',
                        model: 'Image',
                      },
                    },
                    {
                      path: 'likes',
                      model: 'User',
                    },

                    {
                      path: 'comments',
                      populate: {
                        path: 'owner',
                        populate: {
                          path: 'avatar',
                        },
                      },
                    },
                  ],
                },
              ])
          )?.posts;

      return res.status(200).json({ msg: 'Success', patchPost, listPost });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error: error.message });
    }
  },
  getAllPost: async (req, res) => {
    try {
      const listPost = await Post.find({ isValid: true })
        .sort({ createdAt: -1 })
        .populate([
          {
            path: 'owner',
            model: 'User',
            populate: {
              path: 'avatar',
              model: 'Image',
            },
          },
          {
            path: 'likes',
          },
          {
            path: 'images',
          },
          {
            path: 'comments',
            populate: {
              path: 'owner',
              populate: {
                path: 'avatar',
              },
            },
          },
        ]);
      return res.status(200).json({ listPost });
    } catch (error) {
      return res.status(500).json({ error: error.message });
    }
  },
  uploadImg: async (req, res) => {
    try {
      const { id } = req.params;
      req.files.forEach(async (file, index) => {
        if (file?.fieldname === 'postImg') {
          const user = await User.findById(req.user.id).populate('images');
          const post = await Post.findById(req.params.id).populate('images');

          const request = {
            file: file,
            path: 'MeoNetwork/Comment',
          };

          const result = await Upload.uploadSingle(request);

          const savedImage = await Image.create(result);

          await User.findByIdAndUpdate(req.user.id, {
            $push: {
              images: {
                $each: [savedImage._id],
              },
            },
          });

          await Post.findByIdAndUpdate(req.params.id, {
            $push: {
              images: {
                $each: [savedImage._id],
              },
            },
          });
        }

        if (req.files.length - 1 === index) {
          res.status(200).json({ msg: 'Upload image success !' });
        }
      });
    } catch (error) {
      console.log(
        '🚀 ~ file: PostHandle.js ~ line 339 ~ uploadImg: ~ error',
        error,
      );
      return res.status(500).json({ error: error.message });
    }
  },
};
